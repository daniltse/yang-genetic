import $ from 'jquery';
class Application{
    constructor(){
        console.log('application start')
        // страница на весь экран
        let $windowHeight = $(window).height();
        $('.main-block').height($windowHeight);
        $('.main-block').on('resize', function () {
            $('.main-block').height($windowHeight);
        })

        //табы

        let $tabs = $('.tab__item')
        $('.tab__button').on('click',function () {
            $('.tab__button').not(this).removeClass('active');
            $(this).addClass('active');
            $tabs.hide().eq($(this).index()).fadeIn();
        })

        // тесты

        $('.test-input').on('click',function () {
            if($(this).hasClass('true')){
                $(this).parent().parent().parent().parent().find('.test__status').removeClass('test__status--quest test__status--false').addClass('test__status--ok');
            }else if($(this).hasClass('false')){
                $(this).parent().parent().parent().parent().find('.test__status').removeClass('test__status--quest test__status--ok').addClass('test__status--false');
            }

        })

        // Попап

        $('.laboratory__popup-close').on('click',function () {
            $('.laboratory__popup').slideToggle();
            $('.laboratory__content').show();
        });

        // лаборатория

        $('.laboratory__button').on('click',function () {
            if($(this).hasClass('true')){
                $(this).toggleClass('ok');
            }else if($(this).hasClass('false')){
                $(this).toggleClass('no');
            }
            let $count = $(this).parent().find('.ok').length;
            console.log($count)
            if($count == 2){
                $(this).parent().parent().find('.laboratory__next').addClass('show');
                $count = 0;
            }
        })
        let index = 1;
        $('.laboratory__button-reset').on('click',function () {
            if(index != 6){
            $(this).parent().find('.laboratory__item').removeClass('show').addClass('hide');
            $(this).parent().find('.laboratory__item').eq(index).removeClass('hide').addClass('show');
            index ++;
            }else if( index == 6) {
               $('.laboratory__item').removeClass('show').addClass('hide');
               $('.laboratory__cloud').hide();
               $('.laboratory__button-reset').hide();
               $('.laboratory__success').show();
            }
        })
    }
}

new Application();
